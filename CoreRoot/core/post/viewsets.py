import logging

from core.post.models import Post
from core.post.serializers import PostSerializer
from core.utils.http_methods import HttpRequests as HR
from core.utils.viewsets import AbstractViewSet
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

logging.basicConfig(level=logging.DEBUG)

# Create your views here.


class PostViewSet(AbstractViewSet):
    http_method_names = (HR.POST.value, HR.GET.value)
    permission_classes = (IsAuthenticated,)
    serializer_class = PostSerializer

    def get_queryset(self):
        return Post.objects.all()

    def get_object(self):
        if self.kwargs["pk"]:
            object = Post.objects.get_object_by_public_id(self.kwargs["pk"])
            self.check_object_permissions(self.request, object)
            return object

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        logging.debug(serializer)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
