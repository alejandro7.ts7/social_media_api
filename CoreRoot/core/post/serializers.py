from core.post.models import Post
from core.user.models import User
from core.user.serializers import UserSerializer
from core.utils.serializers import AuditAbstractSerializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError


class PostSerializer(AuditAbstractSerializer):
    author = serializers.SlugRelatedField(
        queryset=User.objects.all(), slug_field="public_id"
    )

    def to_representation(self, instance: Post):
        representation = super().to_representation(instance)
        author: User = User.objects.get_object_by_public_id(representation["author"])
        representation["author"] = UserSerializer(author).data
        return representation

    def validate_author(self, value: str):
        if self.context["request"].user != value:
            raise ValidationError("You can't create a post for another user.")
        return value

    class Meta:
        model = Post
        # List of all the fields that can be included in a
        # request or a response
        fields = [
            "id",
            "author",
            "body",
            "edited",
            "created",
            "updated",
        ]
        read_only_fields = ["edited"]
