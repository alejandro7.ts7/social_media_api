from enum import Enum


class HttpRequests(Enum):
    GET = "get"
    PATCH = "patch"
    UPDATE = "update"
    DELETE = "delete"
    POST = "post"
    HEAD = "head"
    OPTIONS = "options"
