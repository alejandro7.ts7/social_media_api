import logging

from core.utils.models.audit import AuditAbstractManager, AuditAbstractModel
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.db import models

logging.basicConfig(level=logging.DEBUG)

# Create your models here.


class UserManager(BaseUserManager, AuditAbstractManager):
    def create_user(
        self,
        username: str,
        email: str,
        password: str = None,
        is_superuser: bool = False,
        is_staff: bool = False,
        **kwargs: dict,
    ):
        """
        Create and return a User with an email, phone number and password.
        """
        if username is None:
            raise TypeError("Users must have a username.")
        if email is None:
            raise TypeError("Users must have an email.")
        if password is None:
            raise TypeError("User must have an email.")

        user = self.model(
            username=username, email=self.normalize_email(email), **kwargs
        )
        user.set_password(password)

        if is_superuser:
            user.is_superuser = True
            user.is_staff = True
        user.save(using=self._db)
        logging.debug(type(user))
        return user


class User(AuditAbstractModel, AbstractBaseUser, PermissionsMixin):
    username = models.CharField(db_index=True, max_length=255, unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(db_index=True, unique=True)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    objects = UserManager()

    def __str__(self):
        return f"{self.email}"

    @property
    def name(self):
        return f"{self.first_name} {self.last_name}"
