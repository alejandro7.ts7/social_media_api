from core.user.models import User
from core.utils.serializers import AuditAbstractSerializer


class UserSerializer(AuditAbstractSerializer):
    class Meta:
        model = User
        fields = "__all__"
        read_only_fields = ["is_active"]
